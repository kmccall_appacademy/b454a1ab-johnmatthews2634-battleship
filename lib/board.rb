class Board
  attr_reader :grid

  def initialize(grid = Board.default_grid)
    @grid = grid
  end

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def display
    print @grid
  end

  def count
    @grid.flatten.count(:s)
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, att)
    row, col = pos
    @grid[row][col] = att
  end

  def empty?(pos = nil)
    row, col = pos
    if pos == nil
      if @grid.flatten.include?(:s)
        return false
      else
        return true
      end
    elsif @grid[row][col] == :s
      return false
    else
      return true
    end
  end

  def won?
    if self.count > 0
      false
    else
      true
    end
  end

  def full?
    if @grid.flatten.include?(nil)
      false
    else
      true
    end
  end

  def place_random_ship
    random_idx = Random.new.rand(@grid.length)
    if self.full?
      raise "Board is full"
    elsif self.empty?
      @grid[random_idx][random_idx] = :s
    else
      until self.full?
        @grid.each_with_index do |arr, arr_idx|
          arr.each_with_index do |pos, pos_idx|
            @grid[arr_idx][pos_idx] = :s
          end
        end
      end
    end
  end



end
