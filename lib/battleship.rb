require_relative "board"
require_relative "player"

class BattleshipGame
  attr_reader :board, :player

  def initialize(player = HumanPlayer.new("John"), board = Board.new)
    @player = player
    @board = board
  end

  def play
    until game_over?
      play_turn
    end
  end

  def play_turn
    board.display
    attack(player.get_play)
  end

  def attack(pos)
    if board[pos] == :s
      board[pos] = :x
      puts "Hit!"
    else
      puts "Miss!"
      board[pos] = :x
    end
  end

  def display_status

  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end
end

if __FILE__ == $PROGRAM_NAME
  BattleshipGame.new.play
end
